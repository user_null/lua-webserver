local log = {}
local function contentFormat(event,logType)
  local e = tostring(event)
io.write('Log Module:\tevent\t\n' .. e ..'\n')
  local lT = tostring(logType)
io.write('Log Module:\tType off log\t\n' .. lT .. '\n')
  local output = '[' .. os.date('At: %I:%M:%S') .. '][' .. lT .. '][' .. e .. ']\n'
io.write('Log Module:\toutput\t\n' .. output .. '\n')  
  return output
end
function log.write(event,logType)
io.write('Called! BEGIN\n')
  local filename = tostring('log_' .. os.date('%d_') .. '.txt')
io.write('Called! LINE 14\n')
  local file = io.open(filename,"a+")
io.write('Called! LINE 16\n')
  file:write(contentFormat(event,logType))
io.write('Called! END\n')
  file:close()
end
return log
