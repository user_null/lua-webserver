# Internet Landlords's Lua WebServer
_Everything is ok._

_Everything will be ok._

___"Good design is as little design as possible."___
- some German motherf\*r

Please visit:
* [Our page](https://internet-landlords.xyz)
* [@kuchikuu's page](https://kuchikuu.xyz)
* [@user_null's page](https://dumpnull.xyz)

## Introduction

This prototype thing made from scratch serve as backend for _internet-landlorsd.xyz_ page, the goal here is to make a Lua backend, as fast as posible, as clean as posible and and smol as posible.

This thing is a project to learn Lua and maybe make a template for more advanced backend in the future.

## Table of content

1. [Rationale](#Why?)
1. [Goals](#Goals)
1. [Usage](#Usage)
1. [Contributing](#Contributing)

## Why?

Because Lua is fast as f\*. LuaJIT is even FASTER (Close to the speed of C).

_But, why don't use a simple Node.js solution? Computing power isn't a problem in 202x_. This has learning porpouses as well, and we try to make this unique.

Also, we are making this as a first experience with a "serious" working enviroment, so, expect weird changes in branches and experimentation.

## Goals

* Short time goal: Working TLS encryption
* Long time goal: __Clean__ well coded template for serving static websites, recive forms, upload files, and various uses. (So our frends could use it easily to server their own websites isn't that great?)

## Usage

The server is designed to be used as a backend to serve files to the Internet.
The server is designed to be very minimal with some nice extra features.

The server requires "socket" module in order to work.
The socket module is not included in this server.


The server can be started simply by running it in lua:
```lua server.lua```

The user can add parameters to the server, which will change its behavior:
```lua server.lua -l -v -p 9999```

This will:
* -l - enable logging
* -v - enable verbose mode
* -p 9999 - set server's port to 9999


**Interpreter.**

The server has a custom lua interpreter built-in.
The interpreter is enabled by default. 

Which means it can run lua scripts saved in *.html files and add the outcome to the content of the page.

**The interpreter uses a custom print function called _echo()_**

**Please use _echo()_ instead of _print()_ to write text to a page**

For example:
```
<html>
	<body>
<%LUA
	echo("Hello!")
LUA%>
	</body>
</html>
```

The ```echo("Hello!")``` will be executed by the interpreter and the outcome will be added to the overall webpage.

**Usage:**

In order to tell the server that a lua code is starting, simply write

```<%LUA``` in a new line.

This **must** be at the very beginning of a new line. You can not add anything else to it.

From this moment, you can write your usual code. 

Please remember to use ```echo()``` instead of ```print()``` in order to print text to a webpage.


When the lua code ends, simply add 

```LUA%>``` 
in a new line, and the interpreter will stop.

Many interpreters can be used inside a file. 


## Contributing

Fork. Change. Push. Make a request. I review. I approve.

### Minor guidelines

* __PascalCase for functions.__
* __lowerCamelCase for generic variables__ (Tables, strings, numbers).
* __file\_name to open a file__ and __REMEMBER__ to close the file, files should always be named in lowerCamelCase.
* __Name booleans like isUp, hasNumber, isClicked.__
* __Follow standart UNIX conventions for commands__ (-v and --version "show version", -h and --help "show help") IS FORBIDDEN TO USE "-comand" or "--c" or even individual commands without hyphen.
* __Try to minimize if-else ladders as much as posible__. learn how to avoid "elses".
* __Include hints about type in a name__: "portNumber", "fileString" , "lookupTable". Lua is not a typed language we need to clean that mess.
* __Event-handling functions should be named like past tense.__
* __Make comments, I'll not remember your commit msg tomorrow__ _Probably I alredy had forgotten about it._

made with (pure hate to bloat) with a grain of <3
